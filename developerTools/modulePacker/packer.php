<?php

	define('CRON', true);

	define('PACKER_WORKING_DIR', __DIR__);

    if (version_compare(phpversion(), '5.3.0', '<')) {
        exit("Версия PHP должна быть старше 5.3.0\n");
    }

    require dirname(__FILE__) . '/packer/Packer.php';
    chdir(dirname(__FILE__) . '/../../');
    require 'standalone.php';

	try {
		$packer = new Packer($argv);
	} catch (Exception $e) {
        exit("Не передан файл конфигурации\n");
	}

	$packer->setExporter(
		new xmlExporter(
			$packer->getConfig('package')
		)
	);

	chdir(dirname(dirname(PACKER_WORKING_DIR)));
	$packer->run();