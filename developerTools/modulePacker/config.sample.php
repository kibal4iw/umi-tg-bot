<?php
/**
 * Module configuration file for UMI exporter.
 * See http://api.docs.umi-cms.ru/razrabotka_nestandartnogo_funkcionala/umimarket/eksporter_modulya_dlya_umimarket/
 * See https://github.com/Convead/umi_convead/wiki/%D0%9E%D0%B1%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD%D0%B0
 *
 */
return array(
    'package' => 'telegram_notifier_panteleev',
    'destination' => './telegram_notifier_panteleev',
    'directories' => array(
        './classes/components/telegram_notifier_panteleev',
    ),
    'files' => array(
        './images/cms/admin/modern/icon/telegram_notifier_panteleev.png',
        './man/ru/telegram_notifier_panteleev/config.html'
    ),
    'installScenario' => './classes/components/telegram_notifier_panteleev/install.php',
);
?>