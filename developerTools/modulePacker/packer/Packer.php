<?php

	/**
	 * Упаковщик решений для umi.cms 2
	 */
	class Packer {

		/**
		 * @var array $config конфигурация упаковщика
		 */
		private $config;
		/**
		 * @var xmlExporter $exporter экспортер
		 */
		private $exporter;
		/**
		 * @var array $objectTypes список типов по модулям
		 */
		private $objectTypes = array();

		/**
		 * Конструктор.
		 * @param array $argv параметры командной строки
		 * @throws RuntimeException в случае если не передана конфигурация
		 */
		public function __construct(array $argv) {
			array_shift($argv);

			if (is_null($argv[0]) && !file_exists($argv[0])) {
				throw new RuntimeException('Not exist configuration.');
			}

            if (!file_exists(realpath($argv[0]))) {
                if (file_exists(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $argv[0])) {
                    $this->config = require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $argv[0];
                    return;
                }
            } else {
                $this->config = require realpath($argv[0]);
                return;
            }

            throw new RuntimeException('Not exist configuration.');
		}

		/**
		 * @param xmlExporter $exporter экспортер
		 */
		public function setExporter(xmlExporter $exporter) {
			$this->exporter = $exporter;
		}

		/**
		 * Возвращает свойство конфигурации.
		 * @param string $name имя свойства
		 * @param mixed $default значение возвращаемое по умолчанию
		 * @return mixed
		 */
		public function getConfig($name, $default = null) {
			return isset($this->config[$name]) ? $this->config[$name] : $default;
		}

		/**
		 * Запускает упаковщик.
		 */
		public function run() {
			if (!$this->exporter instanceof xmlExporter) {
				throw new RuntimeException('Not injected xmlExporter');
			}

			if (is_null($this->getConfig('package'))) {
				throw new RuntimeException('Field "package" is required.');
			}
			
			$savedRelations = $this->getConfig('savedRelations');
			if (is_array($savedRelations)) {
				$this->exporter->setIgnoreRelations($savedRelations);
			} else {
				$this->exporter->setIgnoreRelations();
			}

			$this->packComponent();

			$destination = $this->getDestinationDir();
			$this->exporter
				->execute()
				->save($destination . '/' . $this->getConfig('package') . '.xml');

			$this->addFileInTar(
				new SplFileInfo(
					$destination . '/' . $this->getConfig('package') . '.xml'
				),
				new PharData(
					$destination . '/' . $this->getConfig('package') . '.tar'
				),
				'./' . $this->getConfig('package') . '.xml'
			);
		}

		/**
		 * Упаковывает компонент.
		 */
		private function packComponent() {

			if (is_null($this->getConfig('directories')) && is_null($this->getConfig('files'))) {
				die('Must be set to one of the fields: "directories" or "files".');
			}

			$this->packDirectories();
			$this->packFiles();
			$this->packRegistry();
			$this->addTypes();
			$this->addDataTypes();
			$this->addObjects();
			$this->addBranchesStructure();
			$this->addLangs();
			$this->addTemplates();
            $this->packScenarios();
		}

		/**
		 * Добавляет директории в файл экспорта.
		 */
		private function addDirectories(array $dirsTar) {
			$destination = $this->getDestinationDir();
			$this->exporter->setDestination($destination);

			$this->exporter->addDirs($dirsTar);
		}

		/**
		 * Добавить список файлов в файл экспорта.
		 */
		private function addFiles(array $files) {
			$destination = $this->getDestinationDir();
			$this->exporter->setDestination($destination);

			$this->exporter->addFiles($files);
		}

		/**
		 * Упаковка директорий.
		 * @throws RuntimeException в случае если незадана директория для выходных файлов
		 * @return string
		 */
		private function packDirectories() {
			$destination = $this->getDestinationDir();

			if (is_null($this->getConfig('directories')) && !is_array($this->getConfig('directories'))) {
				return;
			}

			$directories = $this->getConfig('directories');

			$pharData = new PharData(
				$destination . '/' . $this->getConfig('package') . '.tar'
			);

			$filesTar = array();
			$dirsTar = array();
			foreach ($directories as $directory) {
				$objects = new RecursiveIteratorIterator(
					new RecursiveDirectoryIterator($directory),
					RecursiveIteratorIterator::SELF_FIRST
				);

				$dirsTar[] = strtr($directory, '\\', '/');

				foreach ($objects as $object) {
					/** @var SplFileInfo $object */
					if (!$object->isDir()) {
						$filesTar[] = $this->addFileInTar($object, $pharData);
					} elseif (
						!in_array($object->getFilename(), array('.', '..'))
					) {
						$dirsTar[] = $object->getPathname();
					}
				}
			}

			$this->addFiles($filesTar);
			$this->addDirectories($dirsTar);
		}

		/**
		 * Упаковка файлов.
		 * @throws RuntimeException в случае если незадана директория для выходных файлов
		 */
		private function packFiles() {
			$destination = $this->getDestinationDir();

			if (is_null($this->getConfig('files')) && !is_array($this->getConfig('files'))) {
				return;
			}

			$files = $this->getConfig('files');

			$pharData = new PharData(
				$destination . '/' . $this->getConfig('package') . '.tar'
			);

			$filesTar = array();
			foreach ($files as $filePath) {
				/** @var SplFileInfo $object */
				$file = new SplFileInfo($filePath);

				if (!$file->isDir()) {
					$filesTar[] = $this->addFileInTar($file, $pharData);
				}
			}

			$this->addFiles($filesTar);
		}

		/**
		 * Возвращает директорию для хранения запакованного решения.
		 * @throws RuntimeException
		 * @return string
		 */
		private function getDestinationDir() {
			$destination = $this->getConfig('destination');

			if (is_null($destination)) {
				throw new RuntimeException('Field "destination" is required.');
			}

			if (!file_exists($destination)) {
				mkdir($destination, 0777, true);
			}

			return $destination;
		}

		/**
		 * Выбирает ключи реестра.
		 * @param string $component имя компонента
		 * @param string $parent_path путь в реестре
		 * @param bool $recursive рекурсивный выбор
		 * @return array
		 */
		private function getRegistryList($component = "core", $parent_path = "//", $recursive = true) {
			if ($component == "core" && strpos($parent_path, "modules") === 0) {
				return array();
			}
			$paths = array();
			$children = regedit::getInstance()->getList($parent_path);

			if (is_array($children)) {
				foreach ($children as $child) {

					if ($parent_path != "//") {
						$child_path = $parent_path . '/' . $child[0];
					} else {
						$child_path = $child[0];
					}
					$paths[] = $child_path;
					if ($recursive) {
						$paths = array_merge($paths, $this->getRegistryList($component, $child_path));
					}
				}
			}

			return $paths;
		}

		/**
		 * Упаковывает типы.
		 */
		private function addTypes() {
			$this->detectComponents();
			$this->sortObjectTypesByModule();

			$types = array();

			if (!is_null($this->getConfig('types'))) {
				$types = array_merge(is_null($types) ? array() : $types, $this->getConfig('types'));
			}

			$this->exporter->addTypes($types);

			foreach ($types as $typeId) {
				$sel = new selector('pages');
				$sel->types('object-type')->id($typeId);
				$pages = $sel->result();
				$this->exporter->addElements($pages);

				$sel = new selector('objects');
				$sel->types('object-type')->id($typeId);
				$objects = $sel->result();
				$this->exporter->addObjects($objects);
			}
			$this->exporter->setShowAllFields(true);
		}

		/**
		 * Строит иерархию типов объектов по модулям.
		 * @param int $parentType
		 */
		private function sortObjectTypesByModule($parentType = 0) {
			$collection = umiObjectTypesCollection::getInstance();
			$types = $collection->getSubTypesList($parentType);
			foreach ($types as $typeId) {
				$type = $collection->getType($typeId);
				$module = $type->getModule();
				if (!$module) {
					$module = "core";
				}
				if (!isset($this->objectTypes[$module])) {
					continue;
				}
				$this->objectTypes[$module][] = $typeId;

				$this->sortObjectTypesByModule($typeId);
			}
		}

		/**
		 * Получает список модулей в системе.
		 */
		private function detectComponents() {
			$this->objectTypes['core'] = array();
			$modulesList = regedit::getInstance()->getList("//modules");
			foreach ($modulesList as $moduleName) {
				list($moduleName) = $moduleName;
				$this->objectTypes[$moduleName] = array();
			}
		}

		/**
		 * Добавляет файлв в Tar архив.
		 * @param SplFileInfo $file добавляемый файл
		 * @param PharData $pharData архив, в который необходимо добавить файл
		 * @param null|string $localPath локальный путь до добавляемого файла
		 * @return array
		 */
		private function addFileInTar(SplFileInfo $file, PharData $pharData, $localPath = null) {
			echo '.';
			if (is_null($localPath)) {
				$localPath = strtr($file->getPathname(), '\\', '/');
			}
			$pharData->addFile(
				$file->getPathname(),
				$localPath
			);

			return $file->getPathname();
		}

		/**
		 * Пакует реестр.
		 */
		private function packRegistry() {
			if (!is_null($this->getConfig('registry')) && is_array($this->getConfig('registry'))) {
				foreach ($this->getConfig('registry') as $module => $registry) {
					$this->exporter->addRegistry(
						$this->getRegistryList(
							$module,
							$registry['path'],
							isset($registry['recursive']) ? $registry['recursive'] : true
						)
					);
				}
			}
		}

        /**
         * Пакует типы полей.
         */
        private function addDataTypes()
        {
            if (!is_null($this->getConfig('fieldTypes')) && is_array($this->getConfig('fieldTypes'))) {
                $types = umiFieldTypesCollection::getInstance();

                $addTypes = array();
                foreach ($this->getConfig('fieldTypes') as $fieldType) {
                    $addTypes[] = $types->getFieldType($fieldType);
                }

                $this->exporter->addDataTypes($addTypes);
            }
        }

        /**
         * Пакует сценарии установки и обновления пакета.
         */
        private function packScenarios()
        {
            if (!is_null($this->getConfig('installScenario'))) {
                $file = new SplFileInfo($this->getConfig('installScenario'));

                $destination = $this->getDestinationDir();

                $pharData = new PharData(
                    $destination . '/' . $this->getConfig('package') . '.tar'
                );

                if (!$file->isDir()) {
                    $filesTar[] = $this->addFileInTar($file, $pharData, './install.php');
                }
            }

            if (!is_null($this->getConfig('updateScenario'))) {
                $file = new SplFileInfo($this->getConfig('updateScenario'));

                $destination = $this->getDestinationDir();

                $pharData = new PharData(
                    $destination . '/' . $this->getConfig('package') . '.tar'
                );

                if (!$file->isDir()) {
                    $filesTar[] = $this->addFileInTar($file, $pharData, './update.php');
                }
            }
        }

        /**
         * Пакует объекты.
         * В конфиге указывается массив идентификаторов объектов.
         */
        private function addObjects()
        {
            $objectsId = array();

            if (!is_null($this->getConfig('objects'))) {
                $objectsId = array_merge(is_null($objectsId) ? array() : $objectsId, $this->getConfig('objects'));
            }

            $objects = umiObjectsCollection::getInstance();

            foreach ($objectsId as $objectId) {
                $this->exporter->addObjects(array(
                    $objects->getObject($objectId)
                ));
            }

            $this->exporter->setShowAllFields(true);
        }

        /**
         * Экспортирует ветки структуры.
         * В конфигурационном файле необходимо указать идентификаторы родительских элементов.
         */
        private function addBranchesStructure()
        {
            $branchesId = array();

            if (!is_null($this->getConfig('branchesStructure'))) {
                $branchesId = array_merge(is_null($branchesId) ? array() : $branchesId, $this->getConfig('branchesStructure'));
            }

            $this->exporter->addBranches($branchesId);
        }

        /**
         * Экспортирует языки.
         */
        private function addLangs()
        {
            $langsId = array();

            if (!is_null($this->getConfig('langs'))) {
                $langsId = array_merge(is_null($langsId) ? array() : $langsId, $this->getConfig('langs'));
            }

            $this->exporter->addLangs($langsId);
        }

        /**
         * Экспортирует шаблоны.
         */
        private function addTemplates()
        {
            $templatesId = array();

            if (!is_null($this->getConfig('templates'))) {
                $templatesId = array_merge(is_null($templatesId) ? array() : $templatesId, $this->getConfig('templates'));
            }

            $this->exporter->addTemplates($templatesId);
        }
    }

